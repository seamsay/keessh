use tui::widgets;
use tui::widgets::Widget;

pub struct Data<'d> {
	database: Box<super::database::Data<'d>>,
	field: &'d str,
}

impl<'d> Data<'d> {
	pub fn new(database: Box<super::database::Data<'d>>, field: &'d str) -> Self {
		Self { database, field }
	}
}

impl<'d, B: tui::backend::Backend> super::View<'d, B> for Data<'d> {
	fn render(&self, mut frame: tui::Frame<B>, area: tui::layout::Rect) {
		widgets::Paragraph::new([widgets::Text::raw(self.field)].iter())
			.wrap(true)
			.render(&mut frame, area);
	}

	fn handle(self: Box<Self>, _: termion::event::Key) -> Box<dyn super::View<'d, B>> {
		self.database
	}
}
