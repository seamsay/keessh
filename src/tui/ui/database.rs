use tui::layout;
use tui::style;
use tui::style::Modifier;
use tui::widgets;
use tui::widgets::Widget;

#[derive(Clone, Debug)]
struct Node<'d> {
	key: &'d str,
	children: Vec<Node<'d>>,
}

#[derive(Debug)]
pub struct Data<'d> {
	database: &'d keepass::Database,
	selections: Vec<usize>,
	children: Vec<Node<'d>>,
}

impl<'d> Data<'d> {
	fn children(&self) -> Vec<&'d str> {
		self.neighbours(&self.selections)
	}

	fn siblings(&self) -> Vec<&'d str> {
		self.neighbours(&self.selections[..self.selections.len() - 1])
	}

	fn neighbours(&self, selections: &[usize]) -> Vec<&'d str> {
		let mut current = &self.children;
		for &selection in selections {
			current = &current[selection].children
		}
		current.iter().map(|n| n.key).collect::<Vec<_>>()
	}

	fn path(&self) -> Vec<&'d str> {
		let mut path = vec![];
		let mut current = &self.children;
		for &selection in &self.selections {
			path.push(current[selection].key);
			current = &current[selection].children;
		}
		path
	}

	fn above_selection(mut self: Box<Self>) -> Box<Self> {
		let last = self.selections.len() - 1;
		if self.selections[last] > 0 {
			self.selections[last] -= 1;
		}
		self
	}

	fn below_selection(mut self: Box<Self>) -> Box<Self> {
		let last = self.selections.len() - 1;
		if self.selections[last] < self.siblings().len() - 1 {
			self.selections[last] += 1;
		}
		self
	}

	fn left_selection(mut self: Box<Self>) -> Box<Self> {
		if self.selections.len() > 1 {
			self.selections.pop();
		}
		self
	}

	fn right_selection(mut self: Box<Self>) -> Box<Self> {
		let children = self.children();
		if !children.is_empty() {
			self.selections.push(0);
		}
		self
	}
}

impl<'d> std::convert::From<&'d keepass::Database> for Data<'d> {
	fn from(database: &'d keepass::Database) -> Self {
		fn group_node(group: &keepass::Group) -> Node {
			Node {
				key: group.name.as_ref(),
				children: {
					let mut nodes = vec![];
					nodes.extend({
						let mut items = group.child_groups.keys().collect::<Vec<_>>();
						items.sort_unstable();
						items.into_iter().map(|k| {
							group_node(
								group
									.child_groups
									.get(k)
									.expect("`k` comes from the `keys` iterator."),
							)
						})
					});
					nodes.extend({
						let mut items = group.entries.keys().collect::<Vec<_>>();
						items.sort_unstable();
						items.into_iter().map(|k| {
							entry_node(
								k,
								group
									.entries
									.get(k)
									.expect("`k` comes from the `keys` iterator."),
							)
						})
					});
					nodes
				},
			}
		}

		fn entry_node<'d>(key: &'d str, entry: &'d keepass::Entry) -> Node<'d> {
			Node {
				key,
				children: {
					let mut keys = entry.fields.keys().collect::<Vec<_>>();
					keys.sort_unstable_by(|l, r| {
						use std::cmp::Ordering::*;
						if l.eq_ignore_ascii_case("title") {
							Less
						} else if r.eq_ignore_ascii_case("title") {
							Greater
						} else if l.eq_ignore_ascii_case("username") {
							Less
						} else if r.eq_ignore_ascii_case("username") {
							Greater
						} else if l.eq_ignore_ascii_case("password") {
							Less
						} else if r.eq_ignore_ascii_case("password") {
							Greater
						} else {
							l.cmp(r)
						}
					});
					keys.iter()
						.map(|k| Node {
							key: k.as_str(),
							children: vec![],
						})
						.collect::<Vec<_>>()
				},
			}
		}

		let children = group_node(&database.root).children;

		Self {
			database,
			selections: vec![0],
			children,
		}
	}
}

impl<'d, B: tui::backend::Backend> super::View<'d, B> for Data<'d> {
	fn render(&self, mut frame: tui::Frame<B>, area: layout::Rect) {
		let columns = layout::Layout::default()
			.direction(layout::Direction::Horizontal)
			.constraints(vec![
				layout::Constraint::Length(30),
				layout::Constraint::Length(30),
				layout::Constraint::Min(0),
			])
			.split(area);

		widgets::List::new(
			self.path()[..self.selections.len() - 1]
				.iter()
				.map(|&n| widgets::Text::raw(n)),
		)
		.block(widgets::Block::default().borders(widgets::Borders::ALL))
		.render(&mut frame, columns[0]);

		widgets::SelectableList::default()
			.block(widgets::Block::default().borders(widgets::Borders::ALL))
			.items(&self.siblings())
			.highlight_style(style::Style::default().modifier(Modifier::REVERSED))
			.select(self.selections.last().copied())
			.render(&mut frame, columns[1]);

		match self.database.root.get(&self.path()) {
			Some(keepass::Node::Group(_)) => {
				widgets::List::new(self.children().iter().map(|&n| widgets::Text::raw(n)))
					.block(widgets::Block::default().borders(widgets::Borders::ALL))
					.render(&mut frame, columns[2]);
			}
			Some(keepass::Node::Entry(entry)) => {
				widgets::Table::new(
					["Key", "Value"].iter(),
					self.children().into_iter().map(|k| {
						widgets::Row::Data(
							vec![
								k,
								entry.get(k).expect("`k` comes from the `keys` iterator."),
							]
							.into_iter(),
						)
					}),
				)
				.block(widgets::Block::default().borders(widgets::Borders::ALL))
				.header_style(style::Style::default().modifier(Modifier::UNDERLINED))
				.widths({
					// `- 2` for the borders, `- 1` for the separator.
					let width = columns[2].width - 2 - 1;
					let keys = (width / 2).min(20);
					&[keys, width - keys]
				})
				.render(&mut frame, columns[2]);
			}
			None => {
				let path = self.path();
				let (parent, current) = path.split_at(self.selections.len() - 1);
				let current = current[0];

				match self.database.root.get(parent) {
					Some(keepass::Node::Entry(entry)) => {
						widgets::Paragraph::new(
							[widgets::Text::raw(
								entry
									.get(current)
									.expect("`path()` was built from the database."),
							)]
							.iter(),
						)
						.block(widgets::Block::default().borders(widgets::Borders::ALL))
						.wrap(true)
						.render(&mut frame, columns[2]);
					}
					_ => log::error!(
						"INTERNAL: Invalid state: {:?} {:?}",
						self.selections,
						self.path(),
					),
				}
			}
		}
	}

	fn handle(self: Box<Self>, key: termion::event::Key) -> Box<dyn super::View<'d, B>> {
		use termion::event::Key::*;

		match key {
			Up => self.above_selection(),
			Down => self.below_selection(),
			Left => self.left_selection(),
			Right => self.right_selection(),
			Char('\n') => {
				let path = self.path();
				let (parent, current) = path.split_at(self.selections.len() - 1);
				let current = current[0];

				let field = match self.database.root.get(parent) {
					Some(keepass::Node::Entry(entry)) => match entry.get(current) {
						Some(field) => field,
						None => {
							log::error!(
								"INTERNAL: Invalid state: {:?} {:?}",
								self.selections,
								self.path()
							);
							return self;
						}
					},
					Some(_) => {
						log::warn!("Invalid action.");
						return self;
					}
					None => {
						log::error!(
							"INTERNAL: Invalid state: {:?} {:?}",
							self.selections,
							self.path()
						);
						return self;
					}
				};

				Box::new(super::field::Data::new(self, field))
			}
			_ => {
				log::warn!("Invalid key: {:?}", key);
				self
			}
		}
	}
}
