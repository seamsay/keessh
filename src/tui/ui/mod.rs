pub mod database;
pub mod field;

pub trait View<'d, B: tui::backend::Backend>: 'd {
	fn render(&self, frame: tui::Frame<B>, area: tui::layout::Rect);
	fn handle(self: Box<Self>, key: termion::event::Key) -> Box<dyn View<'d, B>>;
}
