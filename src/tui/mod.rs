use termion::raw::IntoRawMode;

mod event;
mod logger;
mod ui;

use ui::View;

// TODO: Take the entire database.
pub fn run(root: &keepass::Database) -> Result<(), Box<dyn std::error::Error>> {
	let mut logs = logger::init(log::Level::Info)?;
	let events = event::EventLoop::default();

	let mut terminal = tui::Terminal::new(tui::backend::TermionBackend::new(
		termion::screen::AlternateScreen::from(std::io::stdout().into_raw_mode()?),
	))?;
	terminal.hide_cursor()?;

	let mut view: Box<dyn View<_>> = Box::new(ui::database::Data::from(root));

	loop {
		terminal.draw(|mut frame| {
			let area = frame.size();
			let area = logs.render(&mut frame, area);
			view.render(frame, area);
		})?;

		match events.receive() {
			Err(_) => log::error!("INTERNAL: Event loop exited prematurely."),
			Ok(Err(e)) => log::error!("Event loop error: {:?}", e),
			Ok(Ok(event::Event::Tick)) => {}
			Ok(Ok(event::Event::Input(key))) => {
				log::trace!("Received key press: {:?}", key);
				view = view.handle(key);
			}
			Ok(Ok(event::Event::Exit)) => {
				log::trace!("Received exit event.");
				break;
			}
		};
	}

	Ok(())
}
