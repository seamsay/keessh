use std::sync::mpsc;
use std::thread;
use std::time;
use termion::event::Key;
use termion::input::TermRead;

macro_rules! send {
	($s:expr, $e:expr) => {
		if $s.send($e).is_err() {
			return;
			}
	};
}

type BoxedError = Box<dyn std::error::Error + Send>;

#[derive(Clone, Debug)]
pub enum Event {
	Exit,
	Input(Key),
	Tick,
}

#[derive(Debug)]
pub struct EventLoop {
	_listener: thread::JoinHandle<()>,
	_ticker: thread::JoinHandle<()>,
	receiver: mpsc::Receiver<Result<Event, BoxedError>>,
}

impl EventLoop {
	fn new(exit_keys: Vec<Key>, tick_period: time::Duration) -> Self {
		let (sender, receiver) = mpsc::channel();

		let _listener = {
			let sender = sender.clone();
			thread::Builder::new()
				.name("key event listener".into())
				.spawn(move || {
					let stdin = std::io::stdin();
					for event in stdin.keys() {
						match event {
							Err(e) => send!(sender, Err(Box::new(e) as BoxedError)),
							Ok(key) => {
								send!(sender, Ok(Event::Input(key)));
								if exit_keys.iter().any(|&exit_key| exit_key == key) {
									send!(sender, Ok(Event::Exit));
								}
							}
						}
					}
				})
				.unwrap()
		};

		let _ticker = {
			let sender = sender.clone();
			thread::Builder::new()
				.name("tick event producer".into())
				.spawn(move || loop {
					send!(sender, Ok(Event::Tick));
					thread::sleep(tick_period);
				})
				.unwrap()
		};

		EventLoop {
			_listener,
			_ticker,
			receiver,
		}
	}

	pub fn receive(&self) -> Result<Result<Event, BoxedError>, std::sync::mpsc::RecvError> {
		self.receiver.recv()
	}
}

impl std::default::Default for EventLoop {
	fn default() -> Self {
		Self::new(
			vec![Key::Esc, Key::Char('q'), Key::Ctrl('c')],
			time::Duration::from_millis(100),
		)
	}
}
