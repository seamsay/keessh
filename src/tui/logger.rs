use std::collections::VecDeque;
use std::sync;
use std::time;
use tui::layout;
use tui::style;
use tui::widgets::{self, Widget};

#[derive(Clone, Debug, Eq)]
struct CapturedLog {
	count: usize,
	duration: time::Duration,
	level: log::Level,
	message: String,
}

impl CapturedLog {
	fn new(record: &log::Record, duration: time::Duration) -> Self {
		Self {
			count: 1,
			duration,
			level: record.level(),
			message: format!("{}", record.args()),
		}
	}
}

impl PartialEq for CapturedLog {
	fn eq(&self, other: &Self) -> bool {
		self.level == other.level && self.message == other.message
	}
}

#[derive(Clone, Debug)]
struct DisplayedLog {
	count: usize,
	expiry: time::Instant,
	level: log::Level,
	message: String,
}

impl DisplayedLog {
	fn render(&self) -> widgets::Text<'static> {
		use log::Level::*;
		use style::Color::*;

		let level = match self.level {
			Trace => "TRACE",
			Debug => "DEBUG",
			Info => "INFO",
			Warn => "WARN",
			Error => "ERROR",
		};

		let mut message = format!("{:5} {}", level, self.message);
		if self.count > 1 {
			message.push_str(&format!(" ({})", self.count));
		}

		let style = style::Style::default().fg(match self.level {
			Trace => Cyan,
			Debug => Green,
			Info => Reset,
			Warn => Yellow,
			Error => Red,
		});

		widgets::Text::styled(message, style)
	}
}

impl std::convert::From<CapturedLog> for DisplayedLog {
	fn from(log: CapturedLog) -> Self {
		let CapturedLog {
			count,
			duration,
			level,
			message,
		} = log;
		Self {
			count,
			level,
			message,
			expiry: time::Instant::now() + duration,
		}
	}
}

impl PartialEq<CapturedLog> for DisplayedLog {
	fn eq(&self, other: &CapturedLog) -> bool {
		self.level == other.level && self.message == other.message
	}
}

type Logs = sync::Arc<sync::Mutex<VecDeque<CapturedLog>>>;

#[derive(Debug)]
pub struct LogStore {
	// TODO: Allow the length of this to be configured, or even dynamic.
	// TODO: Allow user to manually expire logs.
	displayed: [Option<DisplayedLog>; 5],
	logs: Logs,
}

impl LogStore {
	fn rotate(&mut self) {
		// TODO: Instead of `unwrap`, reset the VecDeque.
		let mut logs = self.logs.lock().unwrap();

		let mut i = 0;
		while i + 1 < logs.len() {
			if logs[i] == logs[i + 1] {
				logs.remove(i + 1);
				logs[i].count += 1;
			} else {
				i += 1;
			}
		}

		let mut displayed = 0;

		for element in &mut self.displayed {
			let remove = if let Some(DisplayedLog { expiry, .. }) = *element {
				expiry <= time::Instant::now()
			} else {
				true
			};

			if remove {
				*element = None;
			} else {
				displayed += 1;
			}
		}

		for i in 0..4 {
			if self.displayed[i].is_none() {
				self.displayed.swap(i, i + 1);
			}
		}

		if displayed > 0 && !logs.is_empty() {
			match self.displayed[displayed - 1] {
				Some(ref mut log) if log == &logs[0] => {
					logs.remove(0);
					log.count += 1;
				}
				Some(_) => {}
				None => unreachable!(),
			}
		}

		for i in displayed..5 {
			let log = logs.pop_front().map(|log| log.into());
			self.displayed[i] = log;
		}
	}

	fn len(&self) -> usize {
		self.displayed.iter().filter(|o| o.is_some()).count()
	}

	// TODO: Allow user to provide the area to log into (i.e. don't split the area they give us).
	//       If user gives us a space that is smaller than the number of currently displayed logs,
	//         put the ones that won't be displayed back at the start of the queue with duration
	//         equal to the (until - now()).
	pub fn render<B: tui::backend::Backend>(
		&mut self,
		mut frame: &mut tui::Frame<B>,
		area: layout::Rect,
	) -> layout::Rect {
		self.rotate();

		let rows = self.len() as u16;

		if rows == 0 {
			return area;
		}

		let layout = layout::Layout::default()
			.direction(layout::Direction::Vertical)
			.constraints(vec![
				layout::Constraint::Min(0),
				// `+ 2` for borders.
				layout::Constraint::Length(rows + 2),
			])
			.split(area);

		widgets::List::new(
			self.displayed
				.iter()
				.filter(|o| o.is_some())
				.map(|log| log.as_ref().expect("Already filtered `None`s.").render()),
		)
		.block(widgets::Block::default().borders(widgets::Borders::ALL))
		.render(&mut frame, layout[1]);

		layout[0]
	}
}

#[derive(Debug)]
struct Logger {
	logs: Logs,
	maximum: log::Level,
}

impl log::Log for Logger {
	fn enabled(&self, metadata: &log::Metadata) -> bool {
		metadata.level() <= self.maximum
	}

	fn log(&self, record: &log::Record) {
		if self.enabled(record.metadata()) {
			use log::Level::*;

			// TODO: Allow the user to configure this.
			let duration = time::Duration::from_millis(match record.level() {
				Error => 20_000,
				Debug => 1_000,
				Trace => 500,
				_ => 5_000,
			});

			// TODO: Instead of `unwrap`, reset the VecDeque.
			self.logs
				.lock()
				.unwrap()
				.push_back(CapturedLog::new(record, duration));
		}
	}

	fn flush(&self) {}
}

pub fn init(level: log::Level) -> Result<LogStore, log::SetLoggerError> {
	let store = LogStore {
		displayed: [None, None, None, None, None],
		logs: sync::Arc::new(sync::Mutex::new(VecDeque::new())),
	};

	log::set_boxed_logger(Box::new(Logger {
		logs: store.logs.clone(),
		maximum: level,
	}))?;
	log::set_max_level(level.to_level_filter());

	Ok(store)
}
