use clap::{crate_authors, crate_description, crate_name, crate_version};
use std::io::Write;

mod tui;

fn main() -> Result<(), Box<dyn std::error::Error>> {
	let mut options = Options::new()?;
	let database = database(
		&options.ip,
		&options.user,
		&options.file,
		(&options.password).as_ref().map(String::as_str),
		(&mut options.keyfile).as_mut(),
	)?;

	if let Some(path) = options.entry {
		if let Some(keepass::Node::Entry(entry)) = database
			.root
			.get(&path.iter().map(AsRef::as_ref).collect::<Vec<_>>())
		{
			println!("Username: {}", entry.get_username().unwrap_or(""));
			println!("Password: {}", entry.get_password().unwrap_or(""));
		} else {
			eprintln!("Invalid entry path!");
		}
	} else {
		tui::run(&database)?;
	}

	Ok(())
}

// TODO: Replace this and rpassword with termion functions.
fn prompt<S: std::fmt::Display>(prompt: S) -> std::io::Result<String> {
	print!("{} ", prompt);
	std::io::stdout().flush()?;

	let mut input = String::new();
	std::io::stdin().read_line(&mut input)?;

	// Remove the trailing newline.
	input.pop();

	Ok(input)
}

// TODO: Replace this with StructOpt?
struct Options {
	ip: String,
	file: std::path::PathBuf,
	entry: Option<Vec<String>>,
	user: String,
	keyfile: Option<std::fs::File>,
	password: Option<String>,
}

impl Options {
	fn new() -> Result<Options, Box<dyn std::error::Error>> {
		let matches = clap::app_from_crate!()
			.arg(
				clap::Arg::with_name("ip")
					.short("i")
					.long("ip")
					.help("IP address of the host you wish to connect to.")
					.takes_value(true),
			)
			.arg(
				clap::Arg::with_name("file")
					.short("f")
					.long("file")
					.help("Path to the database file on the server.")
					.takes_value(true),
			)
			.arg(
				clap::Arg::with_name("entry")
					.short("e")
					.long("entry")
					.help("Path to the entry in the database.")
					.long_help(
						"Path to the entry in the database.

	Omitting this will enter interactive mode.

	This option expects the path to be seperated by dots, e.g.:
		'some group.some other group.entry that you want'
	",
					)
					.use_delimiter(true)
					.value_delimiter("."),
			)
			.arg(
				clap::Arg::with_name("user")
					.short("u")
					.long("user")
					.help("User to log in to the remote host as.")
					.env("USER"),
			)
			.arg(
				clap::Arg::with_name("port")
					.short("P")
					.long("port")
					.help("SSH port on the server if it is non-standard.")
					.default_value("22"),
			)
			.arg(
				clap::Arg::with_name("keyfile")
					.short("k")
					.long("keyfile")
					.help("Path to a keyfile.")
					.takes_value(true),
			)
			.arg(
				clap::Arg::with_name("password")
					.short("p")
					.long("password")
					.help("Provide pasword without prompting.")
					.takes_value(true),
			)
			.about("Obtain information from KeePass databases stored on SSH servers.")
			.help_message("Print short (-h) or long (--help) help information and quit.")
			.version_message("Print version information and quit.")
			.get_matches();

		Ok(Options {
			ip: format!(
				"{}:{}",
				if let Some(ip) = matches.value_of("ip") {
					ip.into()
				} else {
					prompt("IP Address:")?
				},
				matches.value_of("port").expect("Defaulted option."),
			),
			file: if let Some(file) = matches.value_of("file") {
				file.into()
			} else {
				prompt("Database File:")?
			}
			.into(),
			entry: matches
				.values_of("entry")
				// TODO: How do I make this less horrible?
				.map(|es| es.map(std::convert::Into::into).collect::<Vec<_>>()),
			user: matches.value_of("user").expect("Defaulted option.").into(),
			keyfile: matches
				.value_of("keyfile")
				.map(std::fs::File::open)
				.transpose()?,
			password: {
				let password = if let Some(password) = matches.value_of("password") {
					password.into()
				} else {
					rpassword::read_password_from_tty(Some("Password: "))?
				};

				if password.is_empty() {
					None
				} else {
					Some(password)
				}
			},
		})
	}
}

fn database(
	ip: &str,
	user: &str,
	file: &std::path::Path,
	password: Option<&str>,
	keyfile: Option<&mut std::fs::File>,
) -> Result<keepass::Database, Box<dyn std::error::Error>> {
	let connection = std::net::TcpStream::connect(ip)?;

	let mut session = ssh2::Session::new()?;
	session.set_tcp_stream(connection);
	session.handshake()?;

	let no_identities_in_agent = -34;
	match session.userauth_agent(user) {
		Err(ref e) if e.code() == no_identities_in_agent => {
			// TODO: Use logging.
			eprintln!("INFO: ssh-agent authentication failed, trying password authentication.");
			// TODO: Use `userauth_keyboard_interactive` instead.
			let password = rpassword::read_password_from_tty(Some("SSH Server Password: "))?;
			session.userauth_password(user, &password)
		}
		other => other,
	}?;

	let (mut file, _stat) = session.scp_recv(file)?;

	keepass::Database::open(
		&mut file,
		password,
		// TODO: Can we make this less horrible?
		keyfile.map(|k| k as &mut dyn std::io::Read),
	)
	.map_err(|e| Box::new(e) as Box<dyn std::error::Error>)
}
